using UnityEditor;
using UnityEngine;

public class UIPanelCreator : Editor
{
    [MenuItem("ARExtention/Record/CreateScreenshotPanel")]
    public static void CreateScreenshotPanel()
    {
        var asset = AssetDatabase.LoadAssetAtPath<GameObject>("Packages/net.array.arextention/Prefabs/Record/ScreenshotPanel.prefab");
        var informationPanel = Instantiate(asset);
        informationPanel.name = "ScreenshotPanel";
    }
    
    [MenuItem("ARExtention/Record/CreatePreviewPanel")]
    public static void CreatePreviewPanel()
    {
        var asset = AssetDatabase.LoadAssetAtPath<GameObject>("Packages/net.array.arextention/Prefabs/Record/PreviewPanel.prefab");
        var informationPanel = Instantiate(asset);
        informationPanel.name = "PreviewPanel";
    }
    
    [MenuItem("ARExtention/Information/CreateInformationPanel")]
    public static void CreateInformationPanel()
    {
        var asset = AssetDatabase.LoadAssetAtPath<GameObject>("Packages/net.array.arextention/Prefabs/Information/InformationPanel.prefab");
        var informationPanel = Instantiate(asset);
        informationPanel.name = "InformationPanel";
    }
    
    [MenuItem("ARExtention/Tutorial/CreateTutorialPanel")]
    public static void CreateTutorialPanel()
    {
        var asset = AssetDatabase.LoadAssetAtPath<GameObject>("Packages/net.array.arextention/Prefabs/Tutorial/TutorialPanel.prefab");
        var informationPanel = Instantiate(asset);
        informationPanel.name = "TutorialPanel";
    }
    
    [MenuItem("ARExtention/Tutorial/CreateTutorialController")]
    public static void CreateTutorialController()
    {
        var asset = AssetDatabase.LoadAssetAtPath<GameObject>("Packages/net.array.arextention/Prefabs/Tutorial/TutorialController.prefab");
        var informationPanel = Instantiate(asset);
        informationPanel.name = "TutorialController";
    }
    
    [MenuItem("ARExtention/Tutorial/CreatePointsPanel")]
    public static void CreatePointsPanel()
    {
        var asset = AssetDatabase.LoadAssetAtPath<GameObject>("Packages/net.array.arextention/Prefabs/Tutorial/PointsPanel.prefab");
        var informationPanel = Instantiate(asset);
        informationPanel.name = "PointsPanel";
    }
    
    [MenuItem("ARExtention/SidePanel/CreateSidePanel")]
    public static void CreateSidePanel()
    {
        var asset = AssetDatabase.LoadAssetAtPath<GameObject>("Packages/net.array.arextention/Prefabs/SidePanel/SidePanel.prefab");
        var informationPanel = Instantiate(asset);
        informationPanel.name = "SidePanel";
    }
}
