# ARExtention

This is a package containing various add-ons for AR projects.

The package contains the following classes:

#### 1. ObjectInteraction.cs 

The class includes functions for interacting with a 3D object on the stage. To use it, you need to add this script to the object as a component.This will allow you to rotate the object and scale it using your fingers. 

#### 2. RecordManager.cs 

The class includes functions to create screens, video recording from the screen. To use it, you need to add a script to any object on the stage and call the following functions:

 - TakeScreenshot();

 - VideoRecord();

#### 3. Share.cs 

The class that includes a function for sharing content to social networks. To use it, you need to add a script to any object on the stage and call the following functions:

- LoadImageAndShare(); - Allows you to select a photo from the gallery and share it.

- LoadVideoAndShare(); - Allows you to select a video from the gallery and share it.

 - Share(string path); -  Allows you to share any content on a given path.

#### 4. UiPanelCreator.cs 

Editor script that allow create some prefabs from up menu.

- CreateInformationPanel(); - Creates on the stage a prefab panel to display information. This prefab needs to be moved to the canvas.

- CreateTutorialPanel(); - Creates a tutorial panel prefab on the stage. This prefab needs to be moved to the canvas.

- CreateTutorialController(); - Creates a prefab on the stage to control the tutorial panels. This prefab must be configured to work!

- CreatePointsPanel(); - Creates a prefab on the stage for displaying points in the training panels. This prefab must be moved to the canvas.

#### 5. InformationPanelScript.cs
 
The class includes functions for working with the information panel.

- ShowAndHidePanel(); - Shows or hides the entire panel.

- HideAndShowSmallPanel(); - Allows you to show or hide a small panel that contains one item.

- HideAndShowScrollPanel(); - Shows or hides the scrolling list bar.

- SetupSmallPanel(GameObject infoObject); - Specifies the content for the small panel.

- SetupScrollPanel(IEnumerable<GameObject> infoObjects); - Sets the content for a scrolling list.

#### 6. TutorialPanel.cs

The class includes functions for working with one training panel.

#### 7. TutorialController.cs

The class includes functions to control the work of all training panels

#### 8. ScanEffect

Contains several classes to work with the screen scanning effect. Look more in the examples.

#### 9. ObjectAppearance.cs

The class includes functions for visualizing the appearance of the subject on the scene.


## Getting Started

Since Unity does not support git links as dependencies, you need to download the following package: 

https://github.com/yasirkula/UnityNativeGallery.git

https://github.com/yasirkula/UnityNativeShare.git

After installing the package, for correct work after the build, you need to transfer the **Plugin** folder from the package directory to the project directory. 

The project contains several examples, by downloading which you can understand how it works.

## Core Contributors

- [Eugeniy Biketov](https://gitlab.com/whiteaangel)
- [Ian Tatiana](https://gitlab.com/iantv)

## License

This project is licensed under the <LICENSE NAME> - see the [LICENSE.md](LICENSE.md) file for details
