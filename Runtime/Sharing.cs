using UnityEngine;

public class Sharing : MonoBehaviour
{
   public void LoadImageAndShare()
   {
      NativeGallery.GetImageFromGallery( ( path ) =>
      {
         Debug.Log( "Image path: " + path );
         if (path == null) return;
         Share(path);
      } ,"Select image to share");
   }
   
   public void LoadVideoAndShare()
   {
      NativeGallery.GetVideoFromGallery( ( path ) =>
      {
         Debug.Log( "Video path: " + path );
         if (path == null) return;
         Share(path);
      }, "Select a video to share");
   }
   
   public void Share(string path)
   {
      new NativeShare().AddFile( path )
         .SetCallback( ( result, shareTarget ) => 
            Debug.Log( "Share result: " + result + ", selected app: " + shareTarget ) )
         .Share();
   }
}
