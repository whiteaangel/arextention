using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;

namespace SidePanel
{
    public class SidePanel : MonoBehaviour, IDragHandler, IBeginDragHandler
    {
        public GameObject panel;

        private float width;
        private float nextSwipeTime;
        
        private enum State
        {
            Left,
            Right
        }

        private State currentState;
        public void Start()
        {
            DOTween.Init();
            width = panel.GetComponent<RectTransform>().rect.width;
        }
        
        private void PanelLeftAndRight()
        {
            if (!(Time.time > nextSwipeTime)) return;
            if (currentState == State.Left)
            {
                currentState = State.Right;
                panel.transform.DOLocalMoveX(0, 1f);
            }
            else
            {
                currentState = State.Left;
                panel.transform.DOLocalMoveX(-width, 1f);
            }
            nextSwipeTime = Time.time + 1;
        }

        public void OnBeginDrag(PointerEventData eventData)
        {
            if (!(Mathf.Abs(eventData.delta.x) > Mathf.Abs(eventData.delta.y))) return;
            if (eventData.delta.x > 0)
            {
                if(currentState != State.Left) return;
                PanelLeftAndRight();
            }
            else
            {
                if(currentState != State.Right) return;
                PanelLeftAndRight();
            }
        }
    
        public void OnDrag(PointerEventData eventData)
        {
        }
    }
}
