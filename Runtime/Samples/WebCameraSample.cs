using System.Collections;
using UnityEngine;
using UnityEngine.Android;
using UnityEngine.UI;

namespace Samples
{
    public class WebCameraSample : MonoBehaviour
    {
        [SerializeField] private RawImage background;
        [SerializeField] private AspectRatioFitter fit;

        private bool isCameraAvailable;
        private WebCamTexture backCam;

        private void Start()
        {
#if PLATFORM_ANDROID
            if (!Permission.HasUserAuthorizedPermission(Permission.Camera))
            {
                Permission.RequestUserPermission(Permission.Camera);
            }
#endif
            StartCoroutine(Delay());
        }

        private IEnumerator Delay()
        {
            yield return new WaitForSeconds(1f);
            var devices = WebCamTexture.devices;
            
            if (devices.Length == 0)
            {
                Debug.Log("Not found camera device");
                isCameraAvailable = false;
                yield break;
            }

            foreach (var t in devices)
            {
                if (!t.isFrontFacing)
                {
                    backCam = new WebCamTexture(t.name, Screen.width, Screen.height);
                }
            }

            if (backCam == null)
            {
                Debug.Log("Cam is null");
                yield break;
            }
            
            backCam.Play();
            background.texture = backCam;
            isCameraAvailable = true;
        }

        private void Update()
        {
            if(!isCameraAvailable)
                return;
            var ratio =  backCam.width / backCam.height;
            fit.aspectRatio = ratio;
            
            var scaleY = backCam.videoVerticallyMirrored ? -1f : 1f;
            background.rectTransform.localScale = new Vector3(1f, scaleY, 1f);

            var orientation = -backCam.videoRotationAngle;
            background.rectTransform.localEulerAngles = new Vector3(0, 0, orientation);
        }
    }
}
