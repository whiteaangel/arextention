using Record;
using UnityEngine;
using UnityEngine.UI;

namespace Samples
{
    public class RecordAndShareSample : MonoBehaviour
    {
        [SerializeField] private RecordManager manager;
        [SerializeField] private Sharing sharing;
        
        [SerializeField] private GameObject sharePanel;
        [SerializeField] private GameObject recordButton;
        public void StartAndStopRecord()
        {
            if(manager.VideoRecord())
                recordButton.GetComponent<Image>().color = recordButton.GetComponent<Image>().color == Color.gray ? Color.red : Color.gray;
        }

        public void TakeScr()
        {
            manager.TakeScreenshot(false);
        }

        public void SharePanelOn()
        {
            sharePanel.SetActive(true);
        }

        public void SharePanelOff()
        {
            sharePanel.SetActive(false);
        }

        public void ShareImage()
        {
            sharing.LoadImageAndShare();
        }

        public void ShareVideo()
        {
            sharing.LoadVideoAndShare();
        }
    }
}
