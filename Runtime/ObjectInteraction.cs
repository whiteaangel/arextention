using UnityEngine;
using UnityEngine.Events;

public class ObjectInteraction : MonoBehaviour
{
    [SerializeField] private float rotateSpeedModifier = 0.1f;
    [SerializeField] private Interaction currentInteraction = Interaction.ScaleAndRotateAndClick;
    [SerializeField] private UnityEvent onClick;

    private const float MINScale = 0.1f;
    private const float MAXScale = 1f;

    private enum Interaction
    {
        None,
        Scale,
        Rotate,
        Click,
        ScaleAndRotateAndClick
    }
    
    private float initialFingersDistance;
    private Vector3 initialScale;

    private void OnMouseDown()
    {
        if(currentInteraction == Interaction.Click || 
           currentInteraction == Interaction.ScaleAndRotateAndClick)
        onClick?.Invoke();    
    }

    private void Update()
    {
        switch (Input.touches.Length)
        {
            case 1:
                if(currentInteraction == Interaction.Rotate || 
                   currentInteraction == Interaction.ScaleAndRotateAndClick)
                    RotateObject();
                break;
            case 2:
                if(currentInteraction == Interaction.Scale || 
                   currentInteraction == Interaction.ScaleAndRotateAndClick)
                    ScaleObject();
                break;
        }
    }
    
    private void RotateObject()
    {
        var touch = Input.GetTouch(0);
        if (touch.phase != TouchPhase.Moved) return;
        var rotation = Quaternion.Euler(0f,-touch.deltaPosition.x * rotateSpeedModifier, 0f);
        transform.rotation = rotation * transform.rotation;
    }

    private void ScaleObject()
    {
        var t1 = Input.touches[0];
        var t2 = Input.touches[1];
           
        if (t1.phase == TouchPhase.Began || t2.phase == TouchPhase.Began)
        {
            initialFingersDistance = Vector2.Distance(t1.position, t2.position);
            initialScale = transform.localScale;
        }
        else if(t1.phase == TouchPhase.Moved || t2.phase == TouchPhase.Moved)
        {
            var currentFingersDistance = Vector2.Distance(t1.position, t2.position);
            var scaleFactor = currentFingersDistance / initialFingersDistance;
            if((initialScale * scaleFactor).x < MINScale || (initialScale * scaleFactor).x > MAXScale)
                return;
            transform.localScale = initialScale * scaleFactor;
        }
    }
}
