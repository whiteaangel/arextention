using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Information
{
    public class InformationPanelScript : MonoBehaviour,IDragHandler, IBeginDragHandler
    {
        public GameObject panel;
        public GameObject smallPanel;
        public GameObject scrollPanel;
        public Transform scrollPanelContent;
        private enum PanelState
        {
            Up,
            Down
        }

        private PanelState currentState;
        private float nextSwipeTime;
        private void Start()
        {
            DOTween.Init();
            currentState = PanelState.Down;
        }

        public void ShowAndHidePanel()
        {
            var canvasGroup = GetComponent<CanvasGroup>();
            if (canvasGroup.alpha >= 1)
            {
                canvasGroup.alpha = 0;
                canvasGroup.blocksRaycasts = false;
                canvasGroup.interactable = false;
            }
            else
            {
                canvasGroup.alpha = 1;
                canvasGroup.blocksRaycasts = true;
                canvasGroup.interactable = true;
            }
        }

        public void SetupSmallPanel(GameObject infoObject)
        {
            foreach (Transform child in smallPanel.transform) 
            {
                Destroy(child.gameObject);
            }
        
            if(scrollPanel.activeSelf )
                HideAndShowScrollPanel();
        
            if(smallPanel.activeSelf == false)
                HideAndShowSmallPanel();
        
            Instantiate(infoObject, smallPanel.transform);
        }

        public void SetupScrollPanel(IEnumerable<GameObject> infoObjects)
        {
            foreach (Transform child in scrollPanelContent.transform) 
            {
                Destroy(child.gameObject);
            }
        
            if(smallPanel.activeSelf )
                HideAndShowSmallPanel();
        
            if(scrollPanel.activeSelf == false)
                HideAndShowScrollPanel();
        
            foreach (var obj in infoObjects)
            {
                Instantiate(obj, scrollPanelContent);
                Destroy(obj);
            }
        }

        public void HideAndShowSmallPanel()
        {
            smallPanel.SetActive(smallPanel.activeSelf == false);
        }

        public void HideAndShowScrollPanel()
        {
            scrollPanel.SetActive(scrollPanel.activeSelf == false);
        }
    
        public void OnBeginDrag(PointerEventData eventData)
        {
            if (Mathf.Abs(eventData.delta.x) > Mathf.Abs(eventData.delta.y))
            {
                //Debug.Log(eventData.delta.x > 0 ? "Right" : "Left");
            }
            else
            {
                if(eventData.delta.y > 0)
                {
                    if (currentState != PanelState.Down) return;
                    PanelUpAndDown();
                }
                else
                {
                    if (currentState != PanelState.Up) return;
                    PanelUpAndDown();
                }
            }
        }

        private void PanelUpAndDown()
        {
            if (!(Time.time > nextSwipeTime)) return;
            if (currentState == PanelState.Down)
            {
                currentState = PanelState.Up;
                panel.transform.DOLocalMoveY(-500f, 1f);
            }
            else
            {
                currentState = PanelState.Down;
                panel.transform.DOLocalMoveY(-1000f, 1f);
            }
            nextSwipeTime = Time.time + 1;
        }

        public void OnDrag(PointerEventData eventData)
        {
        }
    
    }
}
