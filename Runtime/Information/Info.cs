using UnityEngine;
using UnityEngine.UI;

namespace Information
{
    public class Info : MonoBehaviour
    {
        public RawImage logoImage;
        public Text headerText;
        public Text descriptionText;
        
        public enum Type
        {
            Image,
            NotImage
        }

        public Type currentType = Type.Image;
        
        public void SetupPanel(Texture2D texture, string header, TextAnchor headerAlignment,  string description , TextAnchor descriptionAlignment)
        {
            logoImage.texture = texture;
            headerText.text = header;
            headerText.alignment = headerAlignment;
            descriptionText.text = description;
            descriptionText.alignment = descriptionAlignment;
        }
        
        public void SetupPanel(string header, TextAnchor headerAlignment,  string description , TextAnchor descriptionAlignment)
        {
            headerText.text = header;
            headerText.alignment = headerAlignment;
            descriptionText.text = description;
            descriptionText.alignment = descriptionAlignment;
        }
    }
}
