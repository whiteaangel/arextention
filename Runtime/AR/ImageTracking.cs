using UnityEngine;
using UnityEngine.XR.ARFoundation;

namespace AR
{
    public class ImageTracking : MonoBehaviour
    {
        [SerializeField] private GameObject prefab;
        
        private ARTrackedImageManager trackedImageManager;
        private GameObject spawnedPrefab;

        private enum State
        {
            Spawned,
            Update
        }

        private void Awake()
        {
            trackedImageManager = GetComponent<ARTrackedImageManager>();
            spawnedPrefab = Instantiate(prefab, Vector3.zero, Quaternion.identity);
            spawnedPrefab.SetActive(false);
        }

        private void OnEnable()
        {
            trackedImageManager.trackedImagesChanged += ImageChanged;
        }
    
        private void OnDisable()
        {
            trackedImageManager.trackedImagesChanged -= ImageChanged;
        }

        private void ImageChanged(ARTrackedImagesChangedEventArgs eventArgs)
        {
            foreach (var trackedImage in eventArgs.added)
            {
                UpdateImage(trackedImage,State.Spawned);
            }
            foreach (var trackedImage in eventArgs.updated)
            {
                UpdateImage(trackedImage, State.Update);
            }
            foreach (var trackedImage in eventArgs.removed)
            {
                spawnedPrefab.SetActive(false);
            }
        }

        private void UpdateImage(Component trackedImage, State currentState)
        {
            var position = trackedImage.transform.position;
            spawnedPrefab.transform.position = position;

            if (currentState != State.Spawned) return;
            spawnedPrefab.SetActive(true);
        }
    }
}
