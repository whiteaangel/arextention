using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

namespace AR
{
    [RequireComponent(typeof(ARRaycastManager))]
    public class PlaceObject : MonoBehaviour
    {
        [SerializeField] private GameObject spawnPrefab;
        [SerializeField] private ARPlaneManager planeManager;

        private GameObject spawnedObject;
        private ARRaycastManager arRaycastManager;
        private static Vector2 _touchPosition;

        private static readonly List<ARRaycastHit> Hits = new List<ARRaycastHit>();

        private void Start()
        {
            arRaycastManager = GetComponent<ARRaycastManager>();
        }

        private void Update()
        {
            if(!TryGetTouchPosition())
                return;
            if (!arRaycastManager.Raycast(_touchPosition, Hits, TrackableType.PlaneWithinPolygon)) return;
            var hitPose = Hits[0].pose;
            if (spawnedObject != null) return;
        
            SetAllPlanesActive(false);
            planeManager.enabled = false;
        
            var spawnPosition = hitPose.position;
            spawnedObject = Instantiate(spawnPrefab, spawnPosition, Quaternion.identity);
        }

        private void SetAllPlanesActive(bool value)
        {
            foreach (var plane in planeManager.trackables)
            {
                plane.gameObject.SetActive(value);
            }
        }

        public void RemoveObject()
        {
            if (spawnedObject == null) return;
            planeManager.enabled = true;
            SetAllPlanesActive(true);  
            Destroy(spawnedObject);
        }

        private static bool TryGetTouchPosition()
        {
            if (Input.touchCount > 0)
            {
                _touchPosition = Input.GetTouch(0).position;
                return true;
            }
            _touchPosition = default;
            return false;
        }
    }
}
