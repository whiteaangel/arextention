﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace Record
{
    [RequireComponent(typeof(VideoRecorder))]
    public class RecordManager : MonoBehaviour
    {
        public Text timerText;
        public GameObject screenshotPanel;
        public GameObject previewPanel;

        private Texture2D screenshotTexture;
        private VideoRecorder videoRecorder;
        private bool isRecording;
        
        private float timeLeft;
        private void Start()
        {
            DOTween.Init();
            videoRecorder = GetComponent<VideoRecorder>();
        }

        private void Update()
        {
            if(isRecording)
                Timer();
        }

        public void TakeScreenshot(bool isHideUI)
        {
            var objects = new GameObject[] { };
            if (isHideUI)
            {
                objects = FindObjectsOfType(typeof(GameObject)) as GameObject[];
                ShowAndHideUI(objects);
            }

            StartCoroutine(TakeScreenshotAndSave(true));
            StartCoroutine(DelayShowAndHideUI(objects));
        }
    
        public bool VideoRecord()
        {
            if (!isRecording)
            {
                if (!videoRecorder.StartRecording()) return false;
                isRecording = true;
                StartCoroutine(TakeScreenshotAndSave(false));
                return true;
            }
            StopRecording();
            return false;
        }
        
        private void StopRecording()
        {
            videoRecorder.StopRecording();
            isRecording = false;
            timerText.text = "";
            timeLeft = 0;
            SetupPreviewPanel(screenshotTexture);
        }
        
        
        private IEnumerator TakeScreenshotAndSave(bool isSave)
        {
            yield return new WaitForEndOfFrame();

            screenshotTexture = new Texture2D( Screen.width, Screen.height, TextureFormat.RGB24, false );
            screenshotTexture.ReadPixels( new Rect( 0, 0, Screen.width, Screen.height ), 0, 0 );
            screenshotTexture.Apply();
           
            if(!isSave)
                yield break;
            
            StartCoroutine(ShowScreenshot(screenshotTexture));
            
            NativeGallery.SaveImageToGallery( screenshotTexture, "ARGOScreen", "Screenshot" + DateTime.Now.Hour + DateTime.Now.Minute +
                                                                               DateTime.Now.Second + ".png", ( success, path ) => 
                Debug.Log( "Media save result: " + success + " " + path ) );
        }

        private IEnumerator ShowScreenshot(Texture2D texture)
        {
            yield return new WaitForSeconds(0.5f);
            screenshotPanel.GetComponent<CanvasGroup>().alpha = 1;
            
            var ratio =  (float)texture.width / texture.height;
            screenshotPanel.GetComponent<AspectRatioFitter>().aspectRatio = ratio;

            screenshotPanel.GetComponent<RectTransform>().sizeDelta = new Vector2(texture.width, texture.height);
            screenshotPanel.GetComponent<RawImage>().texture = texture;
            
            yield return new WaitForSeconds(1f);
            StartCoroutine(FadeOut(screenshotPanel.GetComponent<CanvasGroup>()));
            yield return new WaitForSeconds(1f);
            SetupPreviewPanel(texture);
        }
        
        private static IEnumerator FadeOut(CanvasGroup canvasGroup)
        {
            while(canvasGroup.alpha > 0.001f) 
            {
                canvasGroup.alpha = Mathf.Lerp(canvasGroup.alpha, 0, 2f * Time.deltaTime);
                yield return null;
            }
        }

        private void SetupPreviewPanel(Texture2D texture2D)
        {
            var squareTexture = GetSquareCenteredTexture(texture2D);
            previewPanel.GetComponent<RawImage>().texture = squareTexture;

            if (previewPanel.GetComponent<RawImage>().color.a <= 1)
            {
                var curColor = previewPanel.GetComponent<RawImage>().color;
                curColor.a = 1;
                previewPanel.GetComponent<RawImage>().color = curColor;
            }
            Destroy(texture2D);
        }
        
        private static Texture2D GetSquareCenteredTexture(Texture2D sourceTexture)
        {
            int squareSize;
            var xPos = 0;
            var yPos = 0;
            if (sourceTexture.height < sourceTexture.width)
            {
                squareSize = sourceTexture.height;
                xPos = (sourceTexture.width - sourceTexture.height) / 2;
            }
            else
            {
                squareSize = sourceTexture.width;
                yPos = (sourceTexture.height - sourceTexture.width) / 2;
            }
             
            var c = ( sourceTexture).GetPixels(xPos, yPos, squareSize, squareSize);
            var croppedTexture = new Texture2D(squareSize, squareSize);
            croppedTexture.SetPixels(c);
            croppedTexture.Apply();
            return croppedTexture;
        }
        
        private static IEnumerator DelayShowAndHideUI(IEnumerable<GameObject> objects)
        {
            yield return new WaitForSeconds(0.5f);
            ShowAndHideUI(objects);
        }
    
        private static void ShowAndHideUI(IEnumerable<GameObject> objects)
        {
            foreach(var obj in objects)
            {
                if(obj.layer == 5)
                {
                    obj.SetActive(!obj.activeSelf);
                }
            } 
        }
        
        private void Timer()
        {
            timeLeft += Time.deltaTime;
            float minutes = Mathf.FloorToInt(timeLeft / 60);
            float seconds = Mathf.FloorToInt(timeLeft % 60);
            if(minutes > 0)
                timerText.text = minutes + " : " + seconds.ToString(CultureInfo.InvariantCulture);
            else
                timerText.text = seconds.ToString(CultureInfo.InvariantCulture);

            if (!(minutes >= 10)) return;
            StopRecording();
        }
        
        private void OnApplicationQuit()
        {
            if(isRecording)
                VideoRecord();
        }

    }
}
