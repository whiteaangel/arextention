using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace Tutorial
{
    public class TutorialController : MonoBehaviour
    {
        public List<TutorialPanel> tutorialPanels;
        public GameObject pointPrefab;
        public Transform firstPointPosition;

        private readonly List<GameObject> points = new List<GameObject>();
        private TutorialPanel currentPanel;
        private int index;
        private float nextSwipeTime;
        private void Start()
        {
            currentPanel = tutorialPanels[0];
            currentPanel.AMoveLeft += MovePanelsLeft;
            currentPanel.AMoveRight += MovePanelsRight;
            
            for (var i = 1; i < tutorialPanels.Count; i++)
            {
                tutorialPanels[i].AMoveLeft += MovePanelsLeft;
                tutorialPanels[i].AMoveRight += MovePanelsRight;
                tutorialPanels[i].MoveRight();
            }
            CreatePoints(tutorialPanels.Count);
        }

        private void MovePanelsLeft()
        {
            if (!(Time.time > nextSwipeTime)) return;
            if (index + 1 > tutorialPanels.Count - 1)
            {
                SkipTutorial();
                return;
            }
            ChangeCurrentPoint(index,new Vector3(1f,1f,1f),Color.blue);
            index++;
            ChangeCurrentPoint(index,new Vector3(1.2f,1.2f,1.2f),Color.red);
            currentPanel.MoveLeft();
            currentPanel = tutorialPanels[index];
            currentPanel.MoveLeft();
            nextSwipeTime = Time.time + 1;
        }

        private void MovePanelsRight()
        {
            if (!(Time.time > nextSwipeTime)) return;
            if (index - 1 < 0)
            {
                return;
            }
            ChangeCurrentPoint(index,new Vector3(1f,1f,1f),Color.blue);
            index--;
            ChangeCurrentPoint(index,new Vector3(1.2f,1.2f,1.2f),Color.red);
            currentPanel.MoveRight();
            currentPanel = tutorialPanels[index];
            currentPanel.MoveRight();
            nextSwipeTime = Time.time + 1;
        }
        
        protected internal void SkipTutorial()
        {
            foreach (var t in tutorialPanels)
            {
                t.gameObject.SetActive(false);
            }

            foreach (var p in points)
            {
                p.SetActive(false);
            }
        }

        private void CreatePoints(int count)
        {
            for (var i = 0; i < count; i++)
            {
                var point = Instantiate(pointPrefab,firstPointPosition);
                points.Add(point);
                if (points.Count > 1)
                {
                    point.transform.localPosition = new Vector3(points[i - 1].transform.localPosition.x + 200,
                        points[i - 1].transform.localPosition.y, points[i - 1].transform.localPosition.z);
                    point.GetComponent<Image>().color = Color.blue;
                }
                else
                {
                    point.GetComponent<Image>().color = Color.red;
                    point.transform.localScale = new Vector3(1.2f, 1.2f, 1.2f);
                }
            }
        }

        private void ChangeCurrentPoint(int i, Vector3 scale, Color color)
        {
            points[i].transform.DOScale(scale, 1);
            points[i].GetComponent<Image>().color = color;
        }
    }
}
