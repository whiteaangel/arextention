using System;
using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Tutorial
{
    public class TutorialPanel : MonoBehaviour,IDragHandler, IBeginDragHandler
    {
        public GameObject skipButton;

        public event Action AMoveLeft;
        public event Action AMoveRight;

        private TutorialController controller;
        private CanvasGroup canvasGroup;
        private void Start()
        {
            controller = GameObject.FindGameObjectWithTag("TutorialController")
                .GetComponent<TutorialController>();
            canvasGroup = GetComponent<CanvasGroup>();
            skipButton.GetComponent<Button>().onClick.AddListener(controller.SkipTutorial);
        }

        public void SetVisible()
        {
            if (canvasGroup.alpha >= 1)
            {
                canvasGroup.alpha = 0;
                canvasGroup.interactable = false;
                canvasGroup.blocksRaycasts = false;
            }
            else
            {
                canvasGroup.alpha = 1;
                canvasGroup.interactable = true;
                canvasGroup.blocksRaycasts = true;
            }
        }

        public void OnBeginDrag(PointerEventData eventData)
        {
            if (!(Mathf.Abs(eventData.delta.x) > Mathf.Abs(eventData.delta.y))) return;
            if (eventData.delta.x > 0)
            {
                AMoveRight?.Invoke();
            }
            else
            {
               AMoveLeft?.Invoke();
            }
        }

        public void MoveRight()
        {
            if (transform.localPosition.x < 0)
                transform.DOLocalMoveX(0, 1f);
            else if(transform.localPosition.x == 0)
                transform.DOLocalMoveX(1920, 1);
        }

        public void MoveLeft()
        {
            if (transform.localPosition.x > 0)
                transform.DOLocalMoveX(0, 1f);
            else if(transform.localPosition.x == 0)
                transform.DOLocalMoveX(-1920, 1);
        }
        
        public void OnDrag(PointerEventData eventData)
        {
        }
    }
}
