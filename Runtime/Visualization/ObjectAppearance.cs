using System;
using DG.Tweening;
using UnityEngine;

namespace Visualization
{
    public class ObjectAppearance : MonoBehaviour
    {
        public enum AppearanceType
        {
            Scale,
            PunchScale,
            ShakeScale
        }

        public AppearanceType currentAppearance = AppearanceType.Scale;
        public Vector3 maxObjectSize = Vector3.one;
        public float appearanceTime = 1f;
      
        private void Awake()
        
        {
            DOTween.Init();
            gameObject.transform.localScale = Vector3.zero;
        }
        
        public void ShowObject()
        {
            switch (currentAppearance)
            {
                case AppearanceType.Scale:
                    ScaleAppearance(maxObjectSize,appearanceTime);
                    break;
                case AppearanceType.PunchScale:
                    PunchScaleAppearance(maxObjectSize,appearanceTime);
                    break;
                case AppearanceType.ShakeScale:
                    ShakeScaleAppearance(appearanceTime,maxObjectSize);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void ScaleAppearance(Vector3 scaleSize, float time)
        {
            gameObject.transform.DOScale(scaleSize,time);
        }
        
        private void PunchScaleAppearance(Vector3 punchScale, float time)
        {
            var sequence = DOTween.Sequence();
            sequence.Append(gameObject.transform.DOScale(new Vector3(1f, 1f, 1f), 0.1f));
            sequence.Append( gameObject.transform.DOPunchScale(punchScale,time,2));
        }

        private void ShakeScaleAppearance(float time, Vector3 strange)
        {
            var sequence = DOTween.Sequence();
            sequence.Append(gameObject.transform.DOScale(new Vector3(1f, 1f, 1f), 0.1f));
            sequence.Append( gameObject.transform.DOShakeScale(time, strange,2,180f));
        }
    }
}
