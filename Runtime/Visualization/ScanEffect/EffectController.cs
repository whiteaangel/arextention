using UnityEngine;

namespace Visualization.ScanEffect
{
    public class EffectController : MonoBehaviour
    {
        public ImageEffect imageEffect;
        public void StartAndStopEffect()
        {
            imageEffect.enabled = !imageEffect.enabled;
        }
    }
}
