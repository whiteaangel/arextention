﻿using UnityEngine;

namespace Visualization.ScanEffect
{
    [CreateAssetMenu(menuName = "Effects/Interference", order = 1)]
    public class InterferenceEffect : BaseEffect
    {
        [SerializeField] private Texture2D interferenceTex;
        [SerializeField] private Shader interferenceShader;

        [SerializeField] private float speed = 0.5f;
        
        public override void OnCreate()
        {
            if (interferenceTex == null)
            {
                interferenceTex = Texture2D.whiteTexture;
            }

            baseMaterial = new Material(interferenceShader);
            baseMaterial.SetTexture("_InterferenceTex", interferenceTex);
            baseMaterial.SetFloat("_Speed", speed);
        }
        
        public override void Render(RenderTexture src, RenderTexture dst)
        {
            Graphics.Blit(src, dst, baseMaterial);
        }
    }
}